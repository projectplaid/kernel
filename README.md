# Plaid kernel

Project Plaid kernel

This code targets the `qemu-aarch64-virt` machine, with custom startup
and linker scripts.

## Build notes

Locally, I use `direnv` to manage environment variables (where to find FPC, etc).
My `.envrc` file looks like:

```
PATH_add $HOME/arm/arm-gnu-toolchain-13.2.Rel1-x86_64-aarch64-none-elf/bin
PATH_add $HOME/fpc/bin
export FPCDIR=$HOME/src/fpc-source
export FPCTARGET=embedded
export FPCTARGETCPU=aarch64
export LAZARUSDIR=/usr/share/lazarus
export PP=$HOME/fpc/bin/ppcrossa64
```

The FPC exports there are so that the [pascal-language-server](https://github.com/genericptr/pascal-language-server)
can find the proper FPC build.

### Editor specific stuff

I'm using KDE's Kate as my primary editor, and here's my LSP Client "User server settings" for
the pascal-language-server:

```
{
    "servers": {
        "pascal": {
            "command": [
                "pasls"
            ],
            "rootIndicationFilePatterns": [
                "Makefile",
                "*.lpi",
                "*.lpk"
            ],
            "url": "https://github.com/genericptr/pascal-language-server",
            "highlightingModeRegex": "^Pascal$"
        }
    }
}
```

## License

MIT License.

The GIC driver code comes from the ARM Trusted Firmware source code, itself
released under the BSD 3-Clause License. It's not a direct copy, but a port
of parts of it to Pascal from C.
