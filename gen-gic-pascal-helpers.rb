#!/usr/bin/env ruby

require "erb"

template = %q{
% registers.each do |r|
  % if r.start_with?("#")
    % next
  % end
  % register, rw = r.split(",")
    function GICD_OFFSET_8_<%= register %>(ID: UInt32): UInt8;
    begin
      GICD_OFFSET_8_<%= register %> := GICD_<%= register %>R + UIntPtr(ID);
    end;

    function GICD_OFFSET_32_<%= register %>(ID: UInt32): UInt32;
    begin
      GICD_OFFSET_32_<%= register %> := GICD_<%= register %>R + ((UIntPtr(ID) shr <%= register %>R_SHIFT) shl 2);
    end;

    function GICD_OFFSET_64_<%= register %>(ID: UInt32): UInt64;
    begin
      GICD_OFFSET_64_<%= register %> := GICD_<%= register %>R + ((UIntPtr(ID) shr <%= register %>R_SHIFT) shl 3);
    end;
    
  % if rw == "rw" || rw == "ro"
    function GICD_READ_8_<%= register %>(Base: UInt32; ID: UInt32): UInt32;
    begin
      GICD_READ_8_<%= register %> := GET8(Base + GICD_OFFSET_8_<%= register %>(ID));
    end;

    function GICD_READ_32_<%= register %>(Base: UInt32; ID: UInt32): UInt32;
    begin
      GICD_READ_32_<%= register %> := GET32(Base + GICD_OFFSET_32_<%= register %>(ID));
    end;

    function GICD_READ_64_<%= register %>(Base: UInt32; ID: UInt32): UInt64;
    begin
      GICD_READ_64_<%= register %> := GET64(Base + GICD_OFFSET_64_<%= register %>(ID));
    end;
  % end

  % if rw == "rw" || rw = "wo"
    procedure GICD_WRITE_8_<%= register %>(Base: UInt32; ID: UInt32; Val: UInt8);
    begin
      PUT8(Base + GICD_OFFSET_8_<%= register %>(ID), Val);
    end;

    procedure GICD_WRITE_32_<%= register %>(Base: UInt32; ID: UInt32; Val: UInt32);
    begin
      PUT32(Base + GICD_OFFSET_32_<%= register %>(ID), Val);
    end;

    procedure GICD_WRITE_64_<%= register %>(Base: UInt32; ID: UInt32; Val: UInt64);
    begin
      PUT64(Base + GICD_OFFSET_64_<%= register %>(ID), Val);
    end;
  % end
% end
}.gsub(/^\s+/, '')

source = ERB.new(template, trim_mode: "%<>")

registers = STDIN.read.split("\n")
puts source.result
