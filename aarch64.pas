unit aarch64;

interface

function Get_Current_EL: UInt32;
function IS_IN_EL0: Boolean;
function IS_IN_EL1: Boolean;
function IS_IN_EL2: Boolean;
function IS_IN_EL3: Boolean;

implementation

const
    MODE_SP_SHIFT   = UInt32($0);
    MODE_SP_MASK    = UInt32($1);
    MODE_SP_EL0 = UInt32($0);
    MODE_SP_ELX = UInt32($1);

    MODE_RW_SHIFT   = UInt32($4);
    MODE_RW_MASK    = UInt32($1);
    MODE_RW_64  = UInt32($0);
    MODE_RW_32  = UInt32($1);

    MODE_EL_SHIFT   = UInt32($2);
    MODE_EL_MASK    = UInt32($3);
    MODE_EL_WIDTH   = UInt32($2);
    MODE_EL3    = UInt32($3);
    MODE_EL2    = UInt32($2);
    MODE_EL1    = UInt32($1);
    MODE_EL0    = UInt32($0);

    MODE32_SHIFT    = UInt32(0);
    MODE32_MASK = UInt32($f);
    MODE32_usr  = UInt32($0);
    MODE32_fiq  = UInt32($1);
    MODE32_irq  = UInt32($2);
    MODE32_svc  = UInt32($3);
    MODE32_mon  = UInt32($6);
    MODE32_abt  = UInt32($7);
    MODE32_hyp  = UInt32($a);
    MODE32_und  = UInt32($b);
    MODE32_sys  = UInt32($f);

procedure dsbishst; external name '_dsbishst';

{$I gicv3regs.inc}

function GET_RW(Mode: UInt32): UInt32; inline;
begin
    GET_RW := ((Mode shr MODE_RW_SHIFT) And MODE_RW_MASK);
end;

function GET_EL(Mode: UInt32): UInt32; inline;
begin
    GET_EL := ((Mode shr MODE_EL_SHIFT) And MODE_EL_MASK);
end;

function GET_SP(Mode: UInt32): UInt32; inline;
begin
    GET_SP := ((Mode shr MODE_SP_SHIFT) And MODE_SP_MASK);
end;

function GET_M32(Mode: UInt32): UInt32; inline;
begin
    GET_M32 := ((Mode shr MODE32_SHIFT) And MODE32_MASK);
end;

function Get_Current_EL: UInt32; inline;
begin
    Get_Current_EL := GET_EL(Read_CurrentEL);
end;

function IS_IN_EL0: Boolean;
begin
    IS_IN_EL0 := GET_EL(Read_CurrentEL) = MODE_EL0;
end;

function IS_IN_EL1: Boolean;
begin
    IS_IN_EL1 := GET_EL(Read_CurrentEL) = MODE_EL1;
end;

function IS_IN_EL2: Boolean;
begin
    IS_IN_EL2 := GET_EL(Read_CurrentEL) = MODE_EL2;
end;

function IS_IN_EL3: Boolean;
begin
    IS_IN_EL3 := GET_EL(Read_CurrentEL) = MODE_EL3;
end;

end.
