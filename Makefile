CFLAGS=-g -O0 -ffreestanding -mcpu=cortex-a72
FPCFLAGS=-Mobjfpc -FEout -Foout -gw -Paarch64 -Tembedded -XPaarch64-none-elf- -Ch268435456 -dKERNDEBUG -Sa
LINKFLAGS=-k-Tlink.ld
QEMU_DEFAULT=-M virt,gic-version=3,virtualization=true,secure=true -cpu cortex-a72 -m 1G -smp 1 -semihosting -kernel out/kernel.elf \
	-serial mon:stdio -nographic -d cpu_reset,guest_errors,int

OBJS = out/start.o \
	out/vectors.o \
	out/gic.o \

UNITS = out/mmio.ppu \
	out/MemoryStream.ppu \
	out/dtb.ppu \
	out/exceptions.ppu \
	out/gicv3.ppu \
	
INCS = gicv3regs.inc \
	gicv3regs.h \
	gicv3helpers.inc \

.PHONY: all clean run

default: all

all: out/kernel.elf

generate: $(INCS)

gicv3regs.inc: gic_registers.csv
	cat gic_registers.csv | ruby gen-gic-pascal.rb > gicv3regs.inc

gicv3regs.h: gic_registers.csv
	cat gic_registers.csv | ruby gen-gic-asm.rb > gicv3regs.h

gicv3helpers.inc: gic_helpers.csv
	cat gic_helpers.csv | ruby gen-gic-pascal-helpers.rb > gicv3helpers.inc

out:
	mkdir -p out

out/%.o: %.S gicv3regs.h
	aarch64-none-elf-gcc $(CFLAGS) -c -o out/$*.o $*.S

out/%.ppu: %.pas gicv3regs.inc
	fpc $(FPCFLAGS) -Cn $*.pas

out/kernel.elf: out $(INCS) $(OBJS) $(UNITS)
# for lots of info about the build, add -va
	fpc $(FPCFLAGS) kernel.pas $(LINKFLAGS)

clean:
	rm -f out/* $(INCS)

out/kernel.img: out/kernel.elf
	aarch64-none-elf-objcopy -O binary out/kernel.elf out/kernel.img

run: out/kernel.img
	qemu-system-aarch64 $(QEMU_DEFAULT) $(QEMU_ARGS)

debug: out/kernel.img
	qemu-system-aarch64 $(QEMU_DEFAULT) -S -s $(QEMU_ARGS)
