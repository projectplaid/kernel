    .global     _wfi
_wfi:
    wfi
    ret

    .global     _irq_enable
_irq_enable:
    msr     DAIFClr, #0xF
    ret

    .global     _irq_disable
_irq_disable:
    msr     DAIFSet, #0xF
    ret

    .global     _set_system_register
_set_system_register:
    msr     ICC_SRE_EL1, x0
    ret

    .global     _spin_lock
_spin_lock:
    mov	w2, #1
    sevl
l1:	wfe
l2:	ldaxr	w1, [x0]
    cbnz	w1, l1
    stxr	w1, w2, [x0]
    cbnz	w1, l2
    ret

   .global    _dsbishst
_dsbishst:
    dsb   ishst
    ret

.macro  read_register   regname
    .global     _read_\regname
_read_\regname:
    mrs     x0, \regname
    ret
.endm

.macro  write_register   regname
    .global     _write_\regname
_write_\regname:
    msr     \regname, x0
    ret
.endm

.macro read_write_register  regname
read_register   \regname
write_register  \regname
.endm

#include "gicv3regs.h"
