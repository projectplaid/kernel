#!/usr/bin/env ruby

require "erb"

template = %q{
% registers.each do |r|
  % if r.start_with?("#")
    % next
  % end
  % register, rw = r.split(",")
  % if rw == "rw" || rw == "ro"
    function Read_<%= register %>: UInt32; external name '_read_<%= register %>';
  % end
  % if rw == "rw" || rw == "wo"
    procedure Write_<%= register %>(Value: UInt32); external name '_write_<%= register %>';
  % end
% end
}.gsub(/^\s+/, '')

source = ERB.new(template, trim_mode: "%<>")

registers = STDIN.read.split("\n")
puts source.result
