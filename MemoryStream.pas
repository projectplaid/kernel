unit MemoryStream;

interface

uses
    classes;

type
    TStreamOnStaticMemory = class(TCustomMemoryStream)
    public
        constructor Create(AMemory: Pointer; ASize: SizeInt);
    end;

implementation

constructor TStreamOnStaticMemory.Create(AMemory: Pointer; ASize: SizeInt);
begin
    inherited Create;
    SetPointer(AMemory, ASize);
end;

end.
