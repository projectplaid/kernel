﻿{- 

UART interface for ARM pl011 UARTs

-}

unit uart;

interface

{- uart is at 0x09000000 for the qemu-aarch64-virt machine -}

procedure UARTInit(BaseAddr: DWord); public name 'UARTInit';
procedure UARTPuts(BaseAddr: DWord; C: AnsiChar);
function UARTGet(BaseAddr: DWord): AnsiChar;
procedure UARTFlush(BaseAddr: DWord);

implementation

uses mmio;

const
    { UART offsets from PeripheralBase }
    UART_DR       = $000;
    UART_FR       = $018;
    UART_IBRD     = $024;
    UART_FBRD     = $028;
    UART_LCRH     = $02C;
    UART_CR       = $030;
    UART_IMSC     = $038;
    UART_ICR      = $044;
    UART_DMACR    = $048;

    CLOCK_RATE = 24000000; // 24 mHz
    BAUD_RATE  = 115200;

procedure UARTInit(BaseAddr: DWord); public name 'UARTInit';
var
    ra: DWord;
    Divisor: UInt32;
    FBaud: UInt32;
    IBaud: UInt32;

begin
    { Turn off the UART first }
    PUT32(BaseAddr + UART_CR, 0);

    { Clear interrupts }
    PUT32(BaseAddr + UART_ICR, $7FF);

    { This is going to assume a clock rate of 24 mHz }
    { divisor = frequency of UART clock / (16 * baud rate) }
    Divisor := Trunc(4 * CLOCK_RATE / BAUD_RATE);
    FBaud := Divisor and $3f;
    IBaud := (Divisor shr 6) and $ffff;

    PUT32(BaseAddr + UART_IBRD, IBaud);
    PUT32(BaseAddr + UART_FBRD, FBaud);

    { 8n1, enable FIFO }
    PUT32(BaseAddr + UART_LCRH, ($7 shl 4));

    { Enable rx and tx }
    PUT32(BaseAddr + UART_CR, $301);
end;

procedure UARTPuts(BaseAddr: DWord; C: AnsiChar);
begin
    while True do
    begin
        DUMMY(1);
        if (GET32(BaseAddr + UART_FR) and $20) = 0 then break;
    end;

    PUT32(BaseAddr + UART_DR, DWord(C));
end;

function UARTGet(BaseAddr: DWord): AnsiChar;
begin
    while True do
    begin
        DUMMY(1);
        if (GET32(BaseAddr + UART_FR) and $10) = 0 then break;
    end;

    UARTGet := AnsiChar(GET32(BaseAddr + UART_DR));
end;

procedure UARTFlush(BaseAddr: DWord);
begin
    PUT32(BaseAddr + UART_LCRH, (1 shl 4));
end;

begin
end.
